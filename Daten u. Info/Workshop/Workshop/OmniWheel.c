// Created on Thu January 7 2016 by G. Koppensteiner
// used as a showcase for the omni-platform

void mavomni(char type, int vel, int time);
// simple function using swith-case for different motorcontrols to achieve different movements 
// movements use just mav- and msleep-functions

void od(int speed, int degree, int time);
// Function OmniDrice (od) is a complexer function based in trigonometry in order to achieve 
// to straight-drive in a certain angel at a different speed for a different time wihtout taking care about moter speeds and without rotation of the omni-platform-base itself
// trigonometry based on THREE OMNI-DIRECTIONAL WHEELS CONTROL ON A MOBILE ROBOT (by F. Ribeiro, I. Moutinho, P. Silva, C. Fraga, N. Pereira) last visited on Jan. 08 2016 at 
// http://ukacc.group.shef.ac.uk/proceedings/control2004/Papers/237.pdf

void drive(int orientation, int rotation, int speed, int time);
// function drive combines od- and mavomni-function to be able to drive straight on a line while also rotating the base +/- 90 degrees.

int main()
{
	int i, orientation = 0; //needed by showcase 4
	printf("Omni-Wheels are Cool!\n");
	
	//showcase 1: DRIVE STRAIGHT TO PERFORM A RECTANGULAR WITHOUT CHANGING ORIENTATION
	printf("Drive straight to perform a circle witout changing orientation!\n");
	od(1000, 0, 1000); //msleep(500);
	od(1000, 90, 1000); //msleep(500);	
	od(1000, 180, 1000); //msleep(500);
	od(1000, 270, 1000); //msleep(500);
	
	//showcase 2: DRIVE STRAIGHT TO PERFORM A RECTANGULAR WITH 90 DEGREE ROTATION WITHOUT MOVEMENT
	printf("Drive straight to perform a circle and change orientation without movement!\n");
	msleep(1000);		
	od(1000, 0, 1000); 
	mavomni(3, 1000, 1200);	
	od(1000, 0, 1000); 	
	mavomni(3, 1000, 1200);	
	od(1000, 0, 1000);
	mavomni(3, 1000, 1200);
	od(1000, 0, 1000);
	mavomni(3, 1000, 1200);
	
	//showcase 3: CHANGE ORIENTATION WHILE DRIVING STRAIGHT
	printf("Drive straight to performe orientation-change at same time to right and left!\n");
	msleep(1000);
	od(1000, 0, 1000); //simply drive straight
	drive(0, 90, 1000, 1200); // follow the direction and change orientation 90 degrees (right)
	msleep(500);
	od(1000, 0, 1000); //simply drive straight
	drive(0, -90, 1000, 1200); // follow the direction and change orientation -90 degrees (left)
	
	//Showcase 4: DRIVE CIRCLEs WITHOUT ROTATING THE BASE
	while(orientation < 360){ //to the right
		od(1000, orientation, 500);
		orientation = orientation +20;
	}
	while(orientation > 0){ //to the left
		od(1000, orientation, 500);
		orientation = orientation -20;
	}
	//Showcase 5: proper use of mavomni for Wheel (Motor) 3
	mavomni(30, 1000, 1000);	
	mavomni(31, 1000, 1000);
	mavomni(32, 1000, 8000);
	mavomni(33, 1000, 4000);
	
	//od(1000, 90, 15000); //simply drive straight
	
	return 0;
}

void mavomni(char type, int vel, int time)
{
	switch (type)
	{
		case 0: //driving forward
		printf("driving forward with mav %i for %i mseconds\n", vel, time);
		mav(1,-vel);
		mav(2,vel);
		msleep(time); 
		ao(); break;
		case 1:	//driving backward
		printf("driving forward with mav %i for %i mseconds\n", vel, time);
		mav(1,vel);
		mav(2,-vel);
		msleep(time);
		ao(); break;
		case 2: //turn clockwise
		printf("turning clockwise with mav %i for %i mseconds\n", vel, time);
		mav(1,vel);
		mav(2,vel);
		mav(3,vel);
		msleep(time); 
		ao(); break;
		case 3:	// turn counterclockwise
		printf("turning counterclockwise with mav %i for %i mseconds\n", vel, time);
		mav(1,-vel);
		mav(2,-vel);
		mav(3,-vel);
		msleep(time); 
		ao(); break;	
		
		
		case 10: // drive forward into direction of Motor 1
		printf("driving forward into direction of moter 1 with mav %i for %i mseconds\n", vel, time);
		mav(3,-vel);
		mav(2,vel);
		msleep(time); 
		ao(); break;
		case 11: // drive 	
		printf("driving backword in direction of moter 1 with mav %i for %i mseconds\n", vel, time);
		mav(3,vel);
		mav(2,-vel);
		msleep(time); 
		ao(); break;
		case 12:
		printf("turning clockwise around moter 1 with mav %i for %i mseconds\n", vel, time);
		//mav(1,-vel);
		mav(2,vel);
		mav(3,vel);
		msleep(time); 
		ao(); break;
		case 13:	
		printf("turning counterclockwise around moter 1 with mav %i for %i mseconds\n", vel, time);
		//mav(1,vel);
		mav(2,-vel);
		mav(3,-vel);
		msleep(time); 
		ao(); break;
		case 20:
		printf("driving forward into direction of moter 2 with mav %i for %i mseconds\n", vel, time);
		mav(1,-vel);
		mav(3,vel);
		msleep(time); 
		ao(); break;
		case 21:	
		printf("driving backwards in direction of moter 2 with mav %i for %i mseconds\n", vel, time);
		mav(1,vel);
		mav(3,-vel);
		msleep(time); 
		ao(); break;
		case 22:
		printf("turning clockwise around moter 2 with mav %i for %i mseconds\n", vel, time);
		//mav(1,-vel);
		mav(1,vel);
		mav(3,vel);
		msleep(time); 
		ao(); break;
		case 23:	
		printf("turning counterclockwise around moter 2 with mav %i for %i mseconds\n", vel, time);
		//mav(1,vel);
		mav(1,-vel);
		mav(3,-vel);
		msleep(time); 
		ao(); break;
		default:
		case 30:
		printf("driving forward into direction of moter 3 with mav %i for %i mseconds\n", vel, time);
		mav(1,-vel);
		mav(2,vel);
		msleep(time); 
		ao(); break;
		case 31:	
		printf("driving forward into direction of moter 3 with mav %i for %i mseconds\n", vel, time);
		mav(1,vel);
		mav(2,-vel);
		msleep(time); 
		ao(); break;
		case 32:
		printf("turning clockwise around moter 3 with mav %i for %i mseconds\n", vel, time);
		//mav(1,-vel);
		mav(1,-vel);
		mav(2,-vel);
		msleep(time); 
		ao(); break;
		case 33:	
		printf("turning counterclockwise around moter 3 with mav %i for %i mseconds\n", vel, time);
		//mav(1,vel);
		mav(2,+vel);
		mav(1,+vel);
		msleep(time); 
		ao(); break;	
		
		
		printf("wrong command\n"); break;
	}
}



void od(int speed, int degree, int time){
	int x, y, m1, m2, m3;
	degree = degree+90;
	x=speed*cos(degree* 3.1415 / 180); 
	y=speed*sin(degree* 3.1415 / 180); printf("x = %i and y = %i \n", x, y);
	m3 = x;
	m1 = x*cos(120* 3.1415 / 180)-y*sin(120* 3.1415 / 180)-80;
	m2 = x*cos(120* 3.1415 / 180)-y*sin(-120* 3.1415 / 180)-80;
	printf("m1 = %i, m2 = %i, m3 =% i\n", m1, m2, m3);
	mav(3, m3);
	mav(1, m1);
	mav(2, m2);
	msleep(time);
	ao();
}

void drive(int orientation, int rotation, int speed, int time){
	int spin_direction = 0;
	int i=0;
	int ori  = orientation;
	int split =10;
	if (rotation < 0) {
		spin_direction =1;
		//rotation = rotation *-1;
	}
	int time_to_rotate = 1000/speed*1200*abs(rotation)/90;
	for (i=0; i<split; i++){
		od(speed, ori, time/split);
		mavomni(2+spin_direction, speed, time_to_rotate/split);
		ori = ori - rotation/split;
	}
	ao();	
}
